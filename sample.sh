#!/bin/bash

# Autor: Paulo Wesley Fogaça Santiago
# Descrição: Instala impressora 
# Requisitos: Safenet, Firefox 51
# Categorias: Impressoras, Softwares, etc
# Tags: impressora, instalação de dispositivo, multifuncional 
# Comentários: Este programa é distribuído na esperança de que seja útil, mas SEM QUALQUER GARANTIA;
# sem sequer a garantia implícita de COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM OBJETIVO ESPECÍFICO.

cat 1>&2 <<-EOF
  Mensagem de abertura do script, se necessário
  Por exemplo: Esse script foi criado com o intuito de realizar tal ação
  Deve ser executado em modo root
  Deve ser executado com login do usuário
  Tem que executar outra coisa antes
EOF

# Variaveis em maiusculo
VERSION="0.7.3"
N_PREFIX=${N_PREFIX-/usr/local}
# Diretorio pra coisa
VERSIONS_DIR=$N_PREFIX/n/versions

# Esta funcao faz tal coisa
function faz_tal_coisa()
{
  # Fazendo tal coisa
  echo "Feito"
}

exit 0
