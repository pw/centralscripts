--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 10.1

-- Started on 2018-01-22 20:48:06 -02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2279 (class 1262 OID 67974)
-- Name: mapadaterapia; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE mapadaterapia WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C.UTF-8';


\connect mapadaterapia

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12390)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 212 (class 1259 OID 108809)
-- Name: chave_valor; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE chave_valor (
    id integer NOT NULL,
    valor character varying(64),
    grupo character varying(32)
);


--
-- TOC entry 211 (class 1259 OID 108807)
-- Name: chave_valor_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE chave_valor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 211
-- Name: chave_valor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE chave_valor_id_seq OWNED BY chave_valor.id;


--
-- TOC entry 190 (class 1259 OID 67858)
-- Name: cidade; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cidade (
    id integer NOT NULL,
    nome character varying(64),
    codigo_ibge character varying(32),
    estado_id integer
);


--
-- TOC entry 189 (class 1259 OID 67856)
-- Name: cidade_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cidade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 189
-- Name: cidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cidade_id_seq OWNED BY cidade.id;


--
-- TOC entry 204 (class 1259 OID 76023)
-- Name: contato; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE contato (
    id integer NOT NULL,
    endereco character varying(64),
    pessoa_id integer,
    servico_id integer
);


--
-- TOC entry 203 (class 1259 OID 76021)
-- Name: contato_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 203
-- Name: contato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contato_id_seq OWNED BY contato.id;


--
-- TOC entry 194 (class 1259 OID 67897)
-- Name: documento; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE documento (
    id integer NOT NULL,
    registro character varying(64),
    principal boolean,
    tipo integer,
    pessoa_id integer,
    cidade_id integer,
    estado_id integer,
    pais_id integer
);


--
-- TOC entry 193 (class 1259 OID 67895)
-- Name: documento_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE documento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 193
-- Name: documento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE documento_id_seq OWNED BY documento.id;


--
-- TOC entry 196 (class 1259 OID 67925)
-- Name: endereco; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE endereco (
    id integer NOT NULL,
    tipo_logradouro integer,
    logradouro character varying(128),
    numero character varying(16),
    numero_interno character varying(16),
    complemento character varying(128),
    bairro character varying(64),
    cep character varying(16),
    cidade_id integer,
    pessoa_id integer,
    latitude double precision,
    longitude double precision
);


--
-- TOC entry 195 (class 1259 OID 67923)
-- Name: endereco_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE endereco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 195
-- Name: endereco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE endereco_id_seq OWNED BY endereco.id;


--
-- TOC entry 188 (class 1259 OID 67845)
-- Name: estado; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE estado (
    id integer NOT NULL,
    nome character varying(64),
    uf character varying(8),
    codigo_ibge character varying(8),
    pais_id integer,
    regiao_id integer
);


--
-- TOC entry 187 (class 1259 OID 67843)
-- Name: estado_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE estado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 187
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE estado_id_seq OWNED BY estado.id;


--
-- TOC entry 206 (class 1259 OID 76041)
-- Name: formacao; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE formacao (
    id integer NOT NULL,
    nome character varying(64),
    descricao character varying(256),
    tipo integer,
    pessoa_id integer,
    professor character varying(128),
    data_conclusao character varying
);


--
-- TOC entry 205 (class 1259 OID 76039)
-- Name: formacao_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE formacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 205
-- Name: formacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE formacao_id_seq OWNED BY formacao.id;


--
-- TOC entry 186 (class 1259 OID 67835)
-- Name: pais; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pais (
    id integer NOT NULL,
    nome character varying(64) DEFAULT NULL::character varying,
    codigo_ddi character varying(8) DEFAULT NULL::character varying
);


--
-- TOC entry 185 (class 1259 OID 67833)
-- Name: pais_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pais_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 185
-- Name: pais_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pais_id_seq OWNED BY pais.id;


--
-- TOC entry 210 (class 1259 OID 108791)
-- Name: perfil; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE perfil (
    id integer NOT NULL,
    pessoa_id integer,
    usuario_id integer
);


--
-- TOC entry 209 (class 1259 OID 108789)
-- Name: perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE perfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 209
-- Name: perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE perfil_id_seq OWNED BY perfil.id;


--
-- TOC entry 192 (class 1259 OID 67871)
-- Name: pessoa; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pessoa (
    id integer NOT NULL,
    nome_principal character varying(64),
    nome_secundario character varying(64),
    tipo integer,
    cidade_id integer,
    pais_id integer,
    data_nascimento character varying
);


--
-- TOC entry 191 (class 1259 OID 67869)
-- Name: pessoa_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pessoa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 191
-- Name: pessoa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pessoa_id_seq OWNED BY pessoa.id;


--
-- TOC entry 200 (class 1259 OID 67961)
-- Name: regiao; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE regiao (
    id integer NOT NULL,
    nome character varying(64)
);


--
-- TOC entry 199 (class 1259 OID 67959)
-- Name: regiao_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE regiao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 199
-- Name: regiao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE regiao_id_seq OWNED BY regiao.id;


--
-- TOC entry 214 (class 1259 OID 116983)
-- Name: servico; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE servico (
    id integer NOT NULL,
    nome character varying(128) NOT NULL,
    formacao_id integer,
    pessoa_id integer,
    tipo integer,
    descricao character varying(256)
);


--
-- TOC entry 202 (class 1259 OID 67977)
-- Name: servico_contato; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE servico_contato (
    id integer NOT NULL,
    nome character varying(64),
    url character varying(64),
    icon character varying(16)
);


--
-- TOC entry 2293 (class 0 OID 0)
-- Dependencies: 202
-- Name: TABLE servico_contato; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE servico_contato IS 'Servico de contato: aplicativos, sites, servico de comunicacao
Ex.: E-mail, facebook, whatsapp';


--
-- TOC entry 201 (class 1259 OID 67975)
-- Name: servico_contato_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE servico_contato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2294 (class 0 OID 0)
-- Dependencies: 201
-- Name: servico_contato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE servico_contato_id_seq OWNED BY servico_contato.id;


--
-- TOC entry 213 (class 1259 OID 116981)
-- Name: servico_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE servico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2295 (class 0 OID 0)
-- Dependencies: 213
-- Name: servico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE servico_id_seq OWNED BY servico.id;


--
-- TOC entry 198 (class 1259 OID 67943)
-- Name: telefone; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE telefone (
    id integer NOT NULL,
    numero character varying(32),
    codigo_ddd character varying(8),
    tipo integer,
    pais_id integer,
    pessoa_id integer
);


--
-- TOC entry 197 (class 1259 OID 67941)
-- Name: telefone_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE telefone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2296 (class 0 OID 0)
-- Dependencies: 197
-- Name: telefone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE telefone_id_seq OWNED BY telefone.id;


--
-- TOC entry 208 (class 1259 OID 84215)
-- Name: usuario; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE usuario (
    id integer NOT NULL,
    nome character varying(32) NOT NULL,
    senha character varying(256) NOT NULL,
    access_token character varying(128),
    auth_key character varying
);


--
-- TOC entry 207 (class 1259 OID 84213)
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2297 (class 0 OID 0)
-- Dependencies: 207
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE usuario_id_seq OWNED BY usuario.id;


--
-- TOC entry 2104 (class 2604 OID 108812)
-- Name: chave_valor id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY chave_valor ALTER COLUMN id SET DEFAULT nextval('chave_valor_id_seq'::regclass);


--
-- TOC entry 2093 (class 2604 OID 67861)
-- Name: cidade id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cidade ALTER COLUMN id SET DEFAULT nextval('cidade_id_seq'::regclass);


--
-- TOC entry 2100 (class 2604 OID 76026)
-- Name: contato id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contato ALTER COLUMN id SET DEFAULT nextval('contato_id_seq'::regclass);


--
-- TOC entry 2095 (class 2604 OID 67900)
-- Name: documento id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY documento ALTER COLUMN id SET DEFAULT nextval('documento_id_seq'::regclass);


--
-- TOC entry 2096 (class 2604 OID 67928)
-- Name: endereco id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY endereco ALTER COLUMN id SET DEFAULT nextval('endereco_id_seq'::regclass);


--
-- TOC entry 2092 (class 2604 OID 67848)
-- Name: estado id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY estado ALTER COLUMN id SET DEFAULT nextval('estado_id_seq'::regclass);


--
-- TOC entry 2101 (class 2604 OID 76044)
-- Name: formacao id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY formacao ALTER COLUMN id SET DEFAULT nextval('formacao_id_seq'::regclass);


--
-- TOC entry 2089 (class 2604 OID 67838)
-- Name: pais id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pais ALTER COLUMN id SET DEFAULT nextval('pais_id_seq'::regclass);


--
-- TOC entry 2103 (class 2604 OID 108794)
-- Name: perfil id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY perfil ALTER COLUMN id SET DEFAULT nextval('perfil_id_seq'::regclass);


--
-- TOC entry 2094 (class 2604 OID 67874)
-- Name: pessoa id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pessoa ALTER COLUMN id SET DEFAULT nextval('pessoa_id_seq'::regclass);


--
-- TOC entry 2098 (class 2604 OID 67964)
-- Name: regiao id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY regiao ALTER COLUMN id SET DEFAULT nextval('regiao_id_seq'::regclass);


--
-- TOC entry 2105 (class 2604 OID 116986)
-- Name: servico id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY servico ALTER COLUMN id SET DEFAULT nextval('servico_id_seq'::regclass);


--
-- TOC entry 2099 (class 2604 OID 67980)
-- Name: servico_contato id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY servico_contato ALTER COLUMN id SET DEFAULT nextval('servico_contato_id_seq'::regclass);


--
-- TOC entry 2097 (class 2604 OID 67946)
-- Name: telefone id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY telefone ALTER COLUMN id SET DEFAULT nextval('telefone_id_seq'::regclass);


--
-- TOC entry 2102 (class 2604 OID 84218)
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY usuario ALTER COLUMN id SET DEFAULT nextval('usuario_id_seq'::regclass);


--
-- TOC entry 2135 (class 2606 OID 108814)
-- Name: chave_valor chave_valor_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY chave_valor
    ADD CONSTRAINT chave_valor_pkey PRIMARY KEY (id);


--
-- TOC entry 2111 (class 2606 OID 67863)
-- Name: cidade cidade_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cidade
    ADD CONSTRAINT cidade_pkey PRIMARY KEY (id);


--
-- TOC entry 2125 (class 2606 OID 76028)
-- Name: contato contato_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contato
    ADD CONSTRAINT contato_pkey PRIMARY KEY (id);


--
-- TOC entry 2115 (class 2606 OID 67902)
-- Name: documento documento_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY (id);


--
-- TOC entry 2117 (class 2606 OID 67930)
-- Name: endereco endereco_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY endereco
    ADD CONSTRAINT endereco_pkey PRIMARY KEY (id);


--
-- TOC entry 2109 (class 2606 OID 67850)
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- TOC entry 2127 (class 2606 OID 76046)
-- Name: formacao formacao_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY formacao
    ADD CONSTRAINT formacao_pkey PRIMARY KEY (id);


--
-- TOC entry 2107 (class 2606 OID 67842)
-- Name: pais pais_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id);


--
-- TOC entry 2133 (class 2606 OID 108796)
-- Name: perfil perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);


--
-- TOC entry 2113 (class 2606 OID 67876)
-- Name: pessoa pessoa_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT pessoa_pkey PRIMARY KEY (id);


--
-- TOC entry 2121 (class 2606 OID 67966)
-- Name: regiao regiao_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY regiao
    ADD CONSTRAINT regiao_pkey PRIMARY KEY (id);


--
-- TOC entry 2123 (class 2606 OID 67982)
-- Name: servico_contato servico_contato_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servico_contato
    ADD CONSTRAINT servico_contato_pkey PRIMARY KEY (id);


--
-- TOC entry 2137 (class 2606 OID 116988)
-- Name: servico servico_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servico
    ADD CONSTRAINT servico_pkey PRIMARY KEY (id);


--
-- TOC entry 2119 (class 2606 OID 67948)
-- Name: telefone telefone_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY telefone
    ADD CONSTRAINT telefone_pkey PRIMARY KEY (id);


--
-- TOC entry 2129 (class 2606 OID 117006)
-- Name: usuario usuario_nome_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_nome_key UNIQUE (nome);


--
-- TOC entry 2131 (class 2606 OID 84220)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2140 (class 2606 OID 67864)
-- Name: cidade cidade_estado_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cidade
    ADD CONSTRAINT cidade_estado_id_fkey FOREIGN KEY (estado_id) REFERENCES estado(id);


--
-- TOC entry 2151 (class 2606 OID 76029)
-- Name: contato contato_pessoa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contato
    ADD CONSTRAINT contato_pessoa_id_fkey FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2152 (class 2606 OID 76034)
-- Name: contato contato_servico_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contato
    ADD CONSTRAINT contato_servico_id_fkey FOREIGN KEY (servico_id) REFERENCES servico_contato(id);


--
-- TOC entry 2144 (class 2606 OID 67908)
-- Name: documento documento_cidade_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT documento_cidade_id_fkey FOREIGN KEY (cidade_id) REFERENCES cidade(id);


--
-- TOC entry 2145 (class 2606 OID 67913)
-- Name: documento documento_estado_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT documento_estado_id_fkey FOREIGN KEY (estado_id) REFERENCES estado(id);


--
-- TOC entry 2146 (class 2606 OID 67918)
-- Name: documento documento_pais_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT documento_pais_id_fkey FOREIGN KEY (pais_id) REFERENCES pais(id);


--
-- TOC entry 2143 (class 2606 OID 67903)
-- Name: documento documento_pessoa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT documento_pessoa_id_fkey FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2147 (class 2606 OID 67931)
-- Name: endereco endereco_cidade_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY endereco
    ADD CONSTRAINT endereco_cidade_id_fkey FOREIGN KEY (cidade_id) REFERENCES cidade(id);


--
-- TOC entry 2148 (class 2606 OID 67936)
-- Name: endereco endereco_pessoa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY endereco
    ADD CONSTRAINT endereco_pessoa_id_fkey FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2138 (class 2606 OID 67851)
-- Name: estado estado_pais_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_pais_id_fkey FOREIGN KEY (pais_id) REFERENCES pais(id);


--
-- TOC entry 2139 (class 2606 OID 67967)
-- Name: estado estado_regiao_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_regiao_id_fkey FOREIGN KEY (regiao_id) REFERENCES regiao(id);


--
-- TOC entry 2142 (class 2606 OID 67882)
-- Name: pessoa fk_pessoa_cidade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT fk_pessoa_cidade FOREIGN KEY (cidade_id) REFERENCES cidade(id);


--
-- TOC entry 2141 (class 2606 OID 67877)
-- Name: pessoa fk_pessoa_pais; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT fk_pessoa_pais FOREIGN KEY (pais_id) REFERENCES pais(id);


--
-- TOC entry 2149 (class 2606 OID 67949)
-- Name: telefone fk_telefone_pais; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY telefone
    ADD CONSTRAINT fk_telefone_pais FOREIGN KEY (pais_id) REFERENCES pais(id);


--
-- TOC entry 2150 (class 2606 OID 67954)
-- Name: telefone fk_telefone_pessoa; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY telefone
    ADD CONSTRAINT fk_telefone_pessoa FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2153 (class 2606 OID 76047)
-- Name: formacao formacao_pessoa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY formacao
    ADD CONSTRAINT formacao_pessoa_id_fkey FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2155 (class 2606 OID 108802)
-- Name: perfil perfil_pessoa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pessoa_id_fkey FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


--
-- TOC entry 2154 (class 2606 OID 108797)
-- Name: perfil perfil_usuario_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_usuario_id_fkey FOREIGN KEY (usuario_id) REFERENCES usuario(id);


--
-- TOC entry 2156 (class 2606 OID 116989)
-- Name: servico servico_formacao_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servico
    ADD CONSTRAINT servico_formacao_id_fkey FOREIGN KEY (formacao_id) REFERENCES formacao(id);


--
-- TOC entry 2157 (class 2606 OID 116994)
-- Name: servico servico_pessoa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servico
    ADD CONSTRAINT servico_pessoa_id_fkey FOREIGN KEY (pessoa_id) REFERENCES pessoa(id);


-- Completed on 2018-01-22 20:48:06 -02

--
-- PostgreSQL database dump complete
--

