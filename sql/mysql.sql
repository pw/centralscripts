CREATE TABLE chave_valor (
    id int(11) NOT NULL,
    valor varchar(64),
    grupo varchar(32)
);

CREATE TABLE cidade (
    id int(11) NOT NULL,
    nome varchar(64),
    codigo_ibge varchar(32),
    estado_id int(11)
);

CREATE TABLE contato (
    id int(11) NOT NULL,
    endereco varchar(64),
    pessoa_id int(11),
    servico_id int(11)
);

CREATE TABLE documento (
    id int(11) NOT NULL,
    registro varchar(64),
    principal bool,
    tipo int(11),
    pessoa_id int(11),
    cidade_id int(11),
    estado_id int(11),
    pais_id int(11)
);

CREATE TABLE endereco (
    id int(11) NOT NULL,
    tipo_logradouro int(11),
    logradouro varchar(128),
    numero varchar(16),
    numero_interno varchar(16),
    complemento varchar(128),
    bairro varchar(64),
    cep varchar(16),
    cidade_id int(11),
    pessoa_id int(11),
    latitude double precision,
    longitude double precision
);

CREATE TABLE estado (
    id int(11) NOT NULL,
    nome varchar(64),
    uf varchar(8),
    codigo_ibge varchar(8),
    pais_id int(11),
    regiao_id int(11)
);

CREATE TABLE formacao (
    id int(11) NOT NULL,
    nome varchar(64),
    descricao text,
    tipo int(11),
    pessoa_id int(11),
    professor varchar(128),
    data_conclusao varchar(255)
);

CREATE TABLE pais (
    id int(11) NOT NULL,
    nome varchar(64) DEFAULT NULL,
    codigo_ddi varchar(8) DEFAULT NULL
);

CREATE TABLE perfil (
    id int(11) NOT NULL,
    pessoa_id int(11),
    usuario_id int(11)
);

CREATE TABLE pessoa (
    id int(11) NOT NULL,
    nome_principal varchar(64),
    nome_secundario varchar(64),
    tipo int(11),
    cidade_id int(11),
    pais_id int(11),
    data_nascimento varchar(255)
);

CREATE TABLE regiao (
    id int(11) NOT NULL,
    nome varchar(64)
);

CREATE TABLE servico (
    id int(11) NOT NULL,
    nome varchar(128) NOT NULL,
    formacao_id int(11),
    pessoa_id int(11),
    tipo int(11),
    descricao text
);

CREATE TABLE servico_contato (
    id int(11) NOT NULL,
    nome varchar(64),
    url varchar(64),
    icon varchar(16)
);

CREATE TABLE telefone (
    id int(11) NOT NULL,
    numero varchar(32),
    codigo_ddd varchar(8),
    tipo int(11),
    pais_id int(11),
    pessoa_id int(11)
);

CREATE TABLE usuario (
    id int(11) NOT NULL,
    nome varchar(32) NOT NULL,
    senha text NOT NULL,
    access_token varchar(128),
    auth_key varchar(255)
);

ALTER TABLE chave_valor
    ADD CONSTRAINT chave_valor_pkey PRIMARY KEY (id);
ALTER TABLE cidade
    ADD CONSTRAINT cidade_pkey PRIMARY KEY (id);
ALTER TABLE contato
    ADD CONSTRAINT contato_pkey PRIMARY KEY (id);
ALTER TABLE documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY (id);
ALTER TABLE endereco
    ADD CONSTRAINT endereco_pkey PRIMARY KEY (id);
ALTER TABLE estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);
ALTER TABLE formacao
    ADD CONSTRAINT formacao_pkey PRIMARY KEY (id);
ALTER TABLE pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id);
ALTER TABLE perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);
ALTER TABLE pessoa
    ADD CONSTRAINT pessoa_pkey PRIMARY KEY (id);
ALTER TABLE regiao
    ADD CONSTRAINT regiao_pkey PRIMARY KEY (id);
ALTER TABLE servico_contato
    ADD CONSTRAINT servico_contato_pkey PRIMARY KEY (id);
ALTER TABLE servico
    ADD CONSTRAINT servico_pkey PRIMARY KEY (id);
ALTER TABLE telefone
    ADD CONSTRAINT telefone_pkey PRIMARY KEY (id);
ALTER TABLE usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
