
TRUNCATE TABLE chave_valor;
INSERT INTO chave_valor (valor, grupo) VALUES('Curso', 'TIPOS_FORMACAO');
INSERT INTO chave_valor (valor, grupo) VALUES('Graduação', 'TIPOS_FORMACAO');
INSERT INTO chave_valor (valor, grupo) VALUES('Pós-graduação', 'TIPOS_FORMACAO');
INSERT INTO chave_valor (valor, grupo) VALUES('Outro', 'TIPOS_FORMACAO');
INSERT INTO chave_valor (valor, grupo) VALUES('Individual', 'TIPOS_SERVICO');
INSERT INTO chave_valor (valor, grupo) VALUES('Aberto', 'TIPOS_SERVICO');
INSERT INTO chave_valor (valor, grupo) VALUES('Apenas convidados', 'TIPOS_SERVICO');
INSERT INTO chave_valor (valor, grupo) VALUES('Celular pessoal', 'TIPOS_TELEFONE');
INSERT INTO chave_valor (valor, grupo) VALUES('Celular empresarial', 'TIPOS_TELEFONE');
INSERT INTO chave_valor (valor, grupo) VALUES('Fixo pessoal', 'TIPOS_TELEFONE');
INSERT INTO chave_valor (valor, grupo) VALUES('Fixo empresarial', 'TIPOS_TELEFONE');
